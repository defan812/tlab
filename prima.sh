echo "Masukan Angka:"
read number
i=2
 
if [ $number -lt 2 ]
then
    echo "$number - bilangan ini bukan bilangan prima."
    exit
fi
 
max=`echo "sqrt($number)" | bc`
while [ $i -le $max ]
do
    if [ `expr $number % $i` -eq 0 ]
    then
        echo "$number - bilangan ini bukan bilangan prima."
        exit
    fi
    i=`expr $i + 1`
done
 
echo "$number -  bilangan ini adalah bilangan prima."